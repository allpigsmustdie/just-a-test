﻿--Автор: doom. VK: https://vk.com/dooms_scripts

local screenWidth, screenHeight = guiGetScreenSize()
local window
local second_window
local window_state = true
local function getWindowPosition(width, height)
    local x = (screenWidth/2) - (width/2) -- центр экрана по ширине
    local y = (screenHeight/1.5) - (height/1.5) -- центр экрана по длине

    return x, y, width, height
end

local function createNewWIndow ()
	if window_state == true then 
		window_state = false
		local x,y,width, height = getWindowPosition(300,300)
		second_window = guiCreateWindow(x,y,width, height, 'FAQ', false)
		local tabPanel = guiCreateTabPanel(0,0.07,1,0.8, true, second_window)
		local tabFaq = guiCreateTab('КОМБИНАЦИИ', tabPanel)
		--- здесь пишется комбинации
		guiWindowSetMovable(second_window, false)-- отключаем перемещение окна
		guiWindowSetSizable(second_window, false)-- Отключаем изменение разрешения окна
		local information = [['Пломбир' = 'Крем 1', 'Крем 2', 'Крем 3'
		'Пломбир_2' = 'Крем 1', 'Крем 2', 'Крем 1'
		'Пломбир_3' = 'Крем 3', 'Крем 1', 'Крем 2']] 
		local tabInformation = guiCreateLabel(0.05, 0.05, 0.9, 1, information, true, tabFaq)
		guiLabelSetHorizontalAlign(tabInformation, 'left', true)
		local tabBtn = guiCreateButton(0, 0.9, 1, 0.1, 'X', true, second_window)
		addEventHandler('onClientGUIClick', tabBtn, function()
			destroyElement(second_window)
			window_state = true
		end)
	end
end
addEvent('start_selling', true)
addEventHandler('start_selling', localPlayer, function(ice_type)
	local x,y,width, height = getWindowPosition(600,400)
	window = guiCreateWindow(x,y,width,height, 'ПРОДАЖА МОРОЖЕННОГО', false)
	showCursor(true)
	--buttons
	local info_button = guiCreateButton(0.85, 0.05, 0.10, 0.10, 'INFO', true, window)
	
	local accept_button = guiCreateButton(0.65, 0.20, 0.15, 0.15, 'ACCEPT', true, window)
	
	local deny_button = guiCreateButton(0.82, 0.20, 0.15, 0.15, '<-', true, window)
	
	local exit_button = guiCreateButton(0.735, 0.35, 0.15, 0.15, 'EXIT', true, window)
	
	local cream1_button = guiCreateButton(0.45, 0.1, 0.15, 0.15, 'CREAM_1', true, window) 
	local cream2_button = guiCreateButton(0.45, 0.3, 0.15, 0.15, 'CREAM_2', true, window) 
	local cream3_button = guiCreateButton(0.45, 0.50, 0.15, 0.15, 'CREAM_3', true, window) 
	local cream4_button = guiCreateButton(0.45, 0.70, 0.15, 0.15, 'CREAM_4', true, window)  
	-----------------------
	local txt = guiCreateLabel(0.65, 0.5, 0.4,0.4, 'Твоя задача - подобрать нужную ', true, window)
	local txt1 = guiCreateLabel(0.65, 0.55, 0.4,0.4, 'комбинацию.', true, window)
	local txt2 = guiCreateLabel(0.65, 0.60, 0.4,0.4, 'Назначения кнопок:', true, window)
	local txt3 = guiCreateLabel(0.65, 0.65, 0.4,0.4, 'INFO - справочник', true, window)
	local txt4 = guiCreateLabel(0.65, 0.70, 0.4,0.4, 'ACCEPT - проверка комбинациии', true, window)
	local txt5 = guiCreateLabel(0.65, 0.75, 0.4,0.4, '<-- удалении элемента в гриде', true, window)
	local txt6 = guiCreateLabel(0.65, 0.80, 0.4,0.4, 'EXIT - выход из игры', true, window)
	-----------------------
	local text_label = guiCreateLabel(0.05, 0.1, 0.2,0.2, 'Текущее мороженное:', true, window)
	local second_text_label = guiCreateLabel(0.05, 0.15, 0.2,0.2, ice_type, true, window)
	-----------------------
	components_list = guiCreateGridList(0.05, 0.4, 0.35, 0.55, true, window)
	guiGridListAddColumn(components_list, 'Компоненты', 0.85)
	--guiGridListAddRow(components_list, 'something')
	guiWindowSetMovable(window, false)-- отключаем перемещение окна
    guiWindowSetSizable(window, false)-- Отключаем изменение разрешения окна
	
	
	addEventHandler('onClientGUIClick', root, function (button, state) -- обработчик события кнопки "ВХОД"
	if button ~= 'left' or state ~= 'up' then -- если !левый клик или !зажатие
		return
	elseif source == info_button then 
		createNewWIndow()
	elseif source == cream1_button then 
		triggerServerEvent('creamButton', localPlayer, 'button_1')
	elseif source == cream2_button then 
		triggerServerEvent('creamButton', localPlayer, 'button_2')
	elseif source == cream3_button then 
		triggerServerEvent('creamButton', localPlayer, 'button_3')
	elseif source == cream4_button then 
		triggerServerEvent('creamButton', localPlayer, 'button_4')
	elseif source == exit_button then
		showCursor(false)
		destroyElement(window)
		if second_window then 
			destroyElement(second_window)
		end
		triggerServerEvent('creamButton', localPlayer, 'button_exit')
	elseif source == deny_button then
		local par = guiGridListGetSelectedItem(components_list)
		local deny = guiGridListRemoveRow(components_list, par)
		triggerServerEvent('creamButton', localPlayer, 'button_delete', par)
	elseif source == accept_button then
		triggerServerEvent('creamButton', localPlayer, 'accept_button')
	end
end)
end)

addEvent('delete_window', true)
addEventHandler('delete_window', localPlayer, function()
	destroyElement(window)
end)

addEvent('plus_row', true)
addEventHandler('plus_row', localPlayer, function(message)
	guiGridListAddRow(components_list, message)
end)



