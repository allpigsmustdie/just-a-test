--check out https://vk.com/dooms_scripts

local main_marker = createMarker(1828.1392822266,-1120.1844482422,23.026389694214, 'cylinder', 1)
local ice_rand
local _cycle_state
local vehicle
local ice_car
--ДОБАВЛЕНИЕ НОВОГО ЗНАЧЕНИЯ(МОРОЖЕННОГО) В ЭТОЙ ТАБЛИЦЕ
local ice_types = {
	{'Пломбир', {'Крем 1', 'Крем 2', 'Крем 3'}},
	{'Пломбир_2', {'Крем 1', 'Крем 2', 'Крем 1'}},
	{'Пломбир_3', {'Крем 3', 'Крем 1', 'Крем 2'}},
	{'Пломбир_4', {'Крем 3', 'Крем 1', 'Крем 2', 'Крем 4'}}
}
-- Таблицы, мне впладу юзать elementData
local work_table = {
}
local ice_table = {
}
--1. Игрок берет задание у педа.
function takeJob(pl)
	if work_table[getPlayerName(pl)..'_work_state'] == false then 
		outputChatBox('Ты уже на работе', 255, 0, 0, pl)
	else
	work_table[getPlayerName(pl)..'_work_state'] = false
	--outputDebugString(toJSON(work_table))
	outputChatBox('Ты взял работу развозчика мороженного, ', pl, 255, 0, 0)
	vehicle = createVehicle(549,1812.6976318359,-1107.1046142578,24.078125)
	warpPedIntoVehicle(pl, vehicle)
	ice_car = getPedOccupiedVehicle(pl)	
	end
end
addEventHandler('onMarkerHit', main_marker, takeJob)

function job_function (pl)
	--getVehicleController (vehicle) == pl
		if getVehicleController (vehicle) == pl and ice_car then 
			ice_table[getPlayerName(pl)] = {} -- лучше использовать свою проверку по аккаунту.
			ice_rand = math.random(1,#ice_types)
			--outputDebugString('trigger 21')
			triggerClientEvent(pl, 'start_selling', pl, ice_types[ice_rand][1])
		end
	end
addCommandHandler('sell', job_function)


addEvent('creamButton', true)
addEventHandler('creamButton', root, function(message, par)
	if message == 'button_1' then
		table.insert(ice_table[getPlayerName(source)], 'Крем 1')
		triggerClientEvent(source, 'plus_row', source, 'Крем_1')
	elseif message == 'button_2' then
		table.insert(ice_table[getPlayerName(source)], 'Крем 2')
		triggerClientEvent(source, 'plus_row', source, 'Крем_2')
	elseif message == 'button_3' then
		table.insert(ice_table[getPlayerName(source)], 'Крем 3')
		triggerClientEvent(source, 'plus_row', source, 'Крем_3')
	elseif message == 'button_4' then 
		table.insert(ice_table[getPlayerName(source)], 'Крем 4')
		triggerClientEvent(source, 'plus_row', source, 'Крем_4')
	elseif message == 'button_exit' then
		--outputDebugString('del')
		ice_table[getPlayerName(source)] = nil
		destroyElement(vehicle)
		outputChatBox('Ты покинул работу, а машина автоматически уехала на стоянку', pl, 255, 0, 0)
		work_table[getPlayerName(source)..'_work_state'] = {}
		work_table[getPlayerName(source)..'_cycle_state'] = {}
	elseif message == 'button_delete' then
		--outputDebugString(par)
		table.remove(ice_table[getPlayerName(source)], par+1)
	elseif message == 'accept_button' then
		for i,v in pairs(ice_table[getPlayerName(source)]) do
			if ice_types[ice_rand][2][i] == v then
				--outputDebugString(ice_table[getPlayerName(source)][i])
				work_table[getPlayerName(source)..'_cycle_state'] = true
			else 
				--outputDebugString('warning')
				work_table[getPlayerName(source)..'_cycle_state'] = false
				break
			end
		end
		if work_table[getPlayerName(source)..'_cycle_state'] == false or work_table[getPlayerName(source)..'_cycle_state'] == nil  then
			outputChatBox('Неправильная комбинация начинка, попробуй снова', pl, 255, 0, 0)
		else
			--outputDebugString(work_table[getPlayerName(source)..'_cycle_state'])
			outputChatBox('Ты создал блестящее мороженное, тобой бы гордились предки.', pl, 188, 104, 167)
			givePlayerMoney(source, 500) -- выдача валюты
			triggerClientEvent(source, 'delete_window', source)
			ice_table[getPlayerName(source)] = {}
			work_table[getPlayerName(source)..'_cycle_state'] = false
			ice_rand = math.random(1,#ice_types)
			triggerClientEvent(source, 'start_selling', source, ice_types[ice_rand][1])
		end
	end
	
end)


